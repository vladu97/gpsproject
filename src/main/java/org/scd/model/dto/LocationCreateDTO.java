package org.scd.model.dto;

import java.time.LocalDateTime;

public class LocationCreateDTO {
    private double latitude;
    private double longitude;
    private LocalDateTime timestamp;

    public LocationCreateDTO()
    {}

    public LocationCreateDTO(double longitude, double latitude, LocalDateTime timestamp)
    {
        this.longitude = longitude;
        this.latitude = latitude;
        this.timestamp = timestamp;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
