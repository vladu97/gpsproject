package org.scd.service;

import org.scd.config.exception.BusinessException;
import org.scd.model.User;
import org.scd.model.Location;
import org.scd.model.dto.UserLoginDTO;
import org.scd.model.dto.UserRegisterDTO;

import java.util.List;
import java.util.Map;

public interface UserService {
    /*
     * Get existing list of users from database
     */
    List<User> getUsers();

    /*
     * Login into application
     */
    User login(final UserLoginDTO userLoginDTO) throws BusinessException;

    User register(final UserRegisterDTO userRegisterDTO) throws BusinessException;

}
