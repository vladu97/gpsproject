package org.scd.service;

import org.scd.config.exception.BusinessException;
import org.scd.model.User;
import org.scd.model.Location;
import org.scd.model.dto.UserLoginDTO;
import org.scd.model.dto.UserRegisterDTO;
import org.scd.model.dto.LocationCreateDTO;

import java.util.List;
import java.util.Map;

public interface LocationService {

    public Location createLocation(LocationCreateDTO locationCreateDTO) throws BusinessException;
}
