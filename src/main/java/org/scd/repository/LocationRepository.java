package org.scd.repository;

import org.springframework.data.repository.CrudRepository;
import org.scd.model.Location;
/**
 * Location Repository
 */
public interface LocationRepository extends CrudRepository<Location, Long> {
    
    Location save(Location location);
}